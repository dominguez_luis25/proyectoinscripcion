﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSSemestre.Models;

namespace MSSemestre.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SemestreController : ControllerBase
    {
        private readonly SemestreDBContext _context;

        public SemestreController(SemestreDBContext context)
        {
            _context = context;
        }

        // GET: api/Semestre
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Semestre>>> GetSemestres()
        {
            return await _context.Semestres.ToListAsync();
        }

        // GET: api/Semestre/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Semestre>> GetSemestre(int id)
        {
            var semestre = await _context.Semestres.FindAsync(id);

            if (semestre == null)
            {
                return NotFound();
            }

            return semestre;
        }

        // PUT: api/Semestre/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSemestre(int id, Semestre semestre)
        {
            if (id != semestre.Id)
            {
                return BadRequest();
            }

            _context.Entry(semestre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SemestreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Semestre
        [HttpPost]
        public async Task<ActionResult<Semestre>> PostSemestre(Semestre semestre)
        {
            _context.Semestres.Add(semestre);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSemestre", new { id = semestre.Id }, semestre);
        }

        // DELETE: api/Semestre/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Semestre>> DeleteSemestre(int id)
        {
            var semestre = await _context.Semestres.FindAsync(id);
            if (semestre == null)
            {
                return NotFound();
            }

            _context.Semestres.Remove(semestre);
            await _context.SaveChangesAsync();

            return semestre;
        }

        private bool SemestreExists(int id)
        {
            return _context.Semestres.Any(e => e.Id == id);
        }
    }
}
