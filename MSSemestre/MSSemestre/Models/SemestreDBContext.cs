﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSSemestre.Models
{
    public class SemestreDBContext:DbContext
    {
        public SemestreDBContext(DbContextOptions<SemestreDBContext> options) : base(options)
        {

        }

        public DbSet<Semestre> Semestres { get; set; }
    }
}
