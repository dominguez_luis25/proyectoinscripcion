﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MSEstudiante.Migrations
{
    public partial class InitialUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fecha_Nacimiento",
                table: "Estudiantes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Fecha_Nacimiento",
                table: "Estudiantes",
                type: "varchar(8)",
                nullable: false,
                defaultValue: "");
        }
    }
}
