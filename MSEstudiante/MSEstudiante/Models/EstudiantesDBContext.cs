﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSEstudiante.Models
{
    public class EstudiantesDBContext : DbContext
    {
        public EstudiantesDBContext(DbContextOptions<EstudiantesDBContext> options) : base(options)
        {

        }

        public DbSet<Estudiante> Estudiantes { get; set; }
    }
}
