﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MSInscripcion.Models
{
    public class Estudiante
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Identificacion { get; set; }
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Nombre { get; set; }
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Apellido { get; set; }
        [Required]
        [Column(TypeName = "varchar(1)")]
        public string Sexo { get; set; }

    }
}
