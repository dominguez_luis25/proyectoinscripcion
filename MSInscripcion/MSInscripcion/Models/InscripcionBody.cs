﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSInscripcion.Models
{
    public class InscripcionBody
    {
        public int Id_Estudiante { get; set; }
        public int Id_Semestre { get; set; }
        public int[] Materias { get; set; }
    }
}
