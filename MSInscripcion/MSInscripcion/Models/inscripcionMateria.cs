﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSInscripcion.Models
{
    public class InscripcionMateria
    {
        [Key]
        public int Id { get; set; }
        public int Id_Inscripcion { get; set; }
        public int Id_Materia { get; set; }
    }
}
