﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSInscripcion.Models
{
    public class Inscripcion
    {
        [Key]
        public int Id { get; set; }
        public int Id_Estudiante { get; set; }
        public int Id_Semestre { get; set; }
    }
}
