﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSInscripcion.Models
{
    public class InscripcionDBContext:DbContext
    {
        public InscripcionDBContext(DbContextOptions<InscripcionDBContext> options) : base(options)
        {

        }

        public DbSet<Inscripcion> Inscripciones { get; set; }
        public DbSet<InscripcionMateria> InscripcionMaterias{ get; set; }
        public DbSet<Estudiante> Estudiantes { get; set; }

    }
}
