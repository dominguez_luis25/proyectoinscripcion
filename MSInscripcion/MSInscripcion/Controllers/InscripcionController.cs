﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSInscripcion.Models;

namespace MSInscripcion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InscripcionController : ControllerBase
    {
        private readonly InscripcionDBContext _context;

        public InscripcionController(InscripcionDBContext context)
        {
            _context = context;
        }

        public class responseItem
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
        }

        // GET: api/Inscripcion
        [HttpGet]
        public async Task<ActionResult<IEnumerable<responseItem>>> GetInscripciones()
        {
           return (from i in _context.Inscripciones
                          join e in _context.Estudiantes on i.Id_Estudiante equals e.Identificacion into Est
                          from m in Est.DefaultIfEmpty()
                          select new responseItem
                          {
                              Id = i.Id,
                              Nombre = m.Nombre
                          }).ToList();

            //return await result;// _context.Inscripciones.ToListAsync();
        }

        // GET: api/Inscripcion/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Inscripcion>> GetInscripcion(int id)
        {
            var inscripcion = await _context.Inscripciones.FindAsync(id);

            if (inscripcion == null)
            {
                return NotFound();
            }

            return inscripcion;
        }

        // PUT: api/Inscripcion/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInscripcion(int id, Inscripcion inscripcion)
        {
            if (id != inscripcion.Id)
            {
                return BadRequest();
            }

            _context.Entry(inscripcion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InscripcionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Inscripcion
        [HttpPost]
        public async Task<ActionResult<Inscripcion>> PostInscripcion(InscripcionBody inscripcionBody)
        {
            Inscripcion nuevaInscripcion = new Inscripcion();
            nuevaInscripcion.Id_Estudiante = inscripcionBody.Id_Estudiante;
            nuevaInscripcion.Id_Semestre = inscripcionBody.Id_Semestre;
            _context.Inscripciones.Add(nuevaInscripcion);
            await _context.SaveChangesAsync();

           
            List<InscripcionMateria> materiasList = new List<InscripcionMateria>();

            foreach (var idMateria in inscripcionBody.Materias)
            {
                InscripcionMateria nuevoInscripMat = new InscripcionMateria();
                nuevoInscripMat.Id_Inscripcion = nuevaInscripcion.Id;
                nuevoInscripMat.Id_Materia = idMateria;
                materiasList.Add(nuevoInscripMat);
            }

            try
            {
                _context.InscripcionMaterias.AddRange(materiasList);
                await _context.SaveChangesAsync();
            }
            catch(Exception err)
            {

            }
            

            return CreatedAtAction("GetInscripcion", new { id = nuevaInscripcion.Id }, nuevaInscripcion);
        }

        // DELETE: api/Inscripcion/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Inscripcion>> DeleteInscripcion(int id)
        {
            var inscripcion = await _context.Inscripciones.FindAsync(id);
            if (inscripcion == null)
            {
                return NotFound();
            }

            _context.Inscripciones.Remove(inscripcion);
            await _context.SaveChangesAsync();

            return inscripcion;
        }

        private bool InscripcionExists(int id)
        {
            return _context.Inscripciones.Any(e => e.Id == id);
        }
    }
}
