﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSMateria.Models
{
    public class MateriasDBContext: DbContext
    {
        public MateriasDBContext(DbContextOptions<MateriasDBContext> options) : base(options)
        {

        }

        public DbSet<Materia> Materias { get; set; }
    }
}
