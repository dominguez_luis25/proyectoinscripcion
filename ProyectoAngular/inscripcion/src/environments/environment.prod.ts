export const environment = {
  production: true,
  URL_BASE: 'http://localhost',
  URL_MS_MATERIA: ':51319/api/Materia',
  URL_MS_ESTUDIANTE: ':51129/api/Estudiante',
  URL_MS_Inscripciones: ':60170/api/Inscripcion'
};
