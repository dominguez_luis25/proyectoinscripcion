import { Component, OnInit } from '@angular/core';
import { MateriasService } from '../services/materias.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styleUrls: ['./materias.component.sass']
})
export class MateriasComponent implements OnInit {

  data: any;

  constructor(private materiasService: MateriasService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.materiasService.get().subscribe((res: any) => {
      console.log("respuesta servicio get materias", res);

      this.data = res;

    },(err:any)=>{
      console.log("error", err);
    },()=>{
     
    });
  }

  onSubmit(form:NgForm)
  {
    console.log(form.value);
    this.materiasService.post(form.value).subscribe(
      res=>{
        this.listar();
      },
      err=>{
        console.log(err);
      }
    );
  }

}
