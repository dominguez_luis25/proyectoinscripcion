import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class InscripcionesService {

  private httpOptions: any = {};

  constructor(private _http: HttpClient,) { }


  get(): Observable<any> {

    let ruta: string = environment.URL_BASE + environment.URL_MS_Inscripciones;
    console.log("service ", ruta);
    this.setHttpOptions();
    
    return this._http.get<any>(ruta, this.httpOptions).pipe(
      catchError(this.handleError<any>(ruta))
    );
  }

  post(formData:any): Observable<any> {

    let ruta: string = environment.URL_BASE + environment.URL_MS_Inscripciones;
    console.log("service ", ruta);
    this.setHttpOptions();
    
    return this._http.post<any>(ruta,formData,this.httpOptions).pipe(
      catchError(this.handleError<any>(ruta))
    );
  }

  private setHttpOptions() {

    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Accept': 'application/json'
        }
      )
    };
  }

  private handleError<T> (operation = 'operation'){
    return (error: any): Observable<any> =>{
      console.error(operation, error); // log to console instead
      alert('ERROR en servicio: '+ operation );
      return of({failed:true});
    };
  }

}
