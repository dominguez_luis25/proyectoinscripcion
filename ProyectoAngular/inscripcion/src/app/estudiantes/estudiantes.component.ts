import { Component, OnInit } from '@angular/core';
import { EstudiantesService } from '../services/estudiantes.service';
import { NgForm } from '@angular/forms';
import { Estudiante } from '../services/estudiante.models';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.sass']
})
export class EstudiantesComponent implements OnInit {
  data: any;
  

  constructor(private estudiantesService: EstudiantesService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.estudiantesService.get().subscribe((res: any) => {
      console.log("respuesta servicio get estudiantes", res);

      this.data = res;

    },(err:any)=>{
      console.log("error", err);
    },()=>{
     
    });
  }

  onSubmit(form:NgForm)
  {
    this.estudiantesService.post(form.value).subscribe(
      res=>{
        this.listar();
      },
      err=>{
        console.log(err);
      }
    );
  }

  populateForm(item:Estudiante){
    this.estudiantesService.formData = item;
  }

}
