import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MateriasComponent } from './materias/materias.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { InscribirComponent } from './inscribir/inscribir.component';

const routes: Routes = [
  {
     path:'materias',
    component:MateriasComponent
  },
  {
    path:'estudiantes',
   component:EstudiantesComponent
 },
  {
    path:'inscripciones',
   component:InscribirComponent
 }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
