import { Component, OnInit } from '@angular/core';
import { EstudiantesService } from '../services/estudiantes.service';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Estudiante } from '../services/estudiante.models';
import { MateriasService } from '../services/materias.service';
import { InscripcionesService } from '../services/inscripciones.service';

@Component({
  selector: 'app-inscribir',
  templateUrl: './inscribir.component.html',
  styleUrls: ['./inscribir.component.sass']
})
export class InscribirComponent implements OnInit {

  dataEstudiante: any;
  dataMateria: any;
  data:any;

  checkoutForm: FormGroup;

  materias: number[] = [];
  

  constructor(private estudiantesService: EstudiantesService,
              private materiasService:MateriasService,
              private inscripcionesService:InscripcionesService,
              private formBuilder: FormBuilder) { 

                this.checkoutForm = this.formBuilder.group({
                  Id_Estudiante: [null, [ Validators.required ] ],
                  Id_Semestre:[null, [ Validators.required ] ],
                });

              }

  ngOnInit() {
    this.listarEstudiantes();
    this.listarMaterias();
    this.listar();
  }

  listarEstudiantes(){
    this.estudiantesService.get().subscribe((res: any) => {
      console.log("respuesta servicio get inscripciones", res);

      this.dataEstudiante = res;

    },(err:any)=>{
      console.log("error", err);
    },()=>{
     
    });
  }

  listarMaterias(){
    this.materiasService.get().subscribe((res: any) => {
      console.log("respuesta servicio get Materias", res);

      this.dataMateria = res;

    },(err:any)=>{
      console.log("error", err);
    },()=>{
     
    });
  }

  onCheckChange(event: any,valor:number){

    console.log(event.target.checked);
    if(event.target.checked == true)
      this.materias.push(valor);
    else{
      let index = this.materias.indexOf(valor);
 
      if (index > -1) {
        this.materias.splice(index, 1);
      }
    }
  
  }

  listar(){
    this.inscripcionesService.get().subscribe((res: any) => {
      console.log("respuesta servicio get inscripcion", res);

      this.data = res;

    },(err:any)=>{
      console.log("error", err);
    },()=>{
     
    });
  }
  
  onSubmit(form:any)
  {
    form.materias = this.materias;
    console.log('form', form)
    this.inscripcionesService.post(form).subscribe(
      (res:any)=>{
        this.listar();
      },
      err=>{
        console.log(err);
      }
    );
  }


}
