import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InscribirComponent } from './inscribir/inscribir.component';
import { MateriasComponent } from './materias/materias.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EstudiantesService } from './services/estudiantes.service';
import { MateriasService } from './services/materias.service';

@NgModule({
  declarations: [
    AppComponent,
    InscribirComponent,
    MateriasComponent,
    EstudiantesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [HttpClient,EstudiantesService,MateriasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
